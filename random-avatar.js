
// BEGIN RANDOM EEPLE

	// This build function takes all possible avatar
	// pieces and builds a completed random avatar

	function randomEepleForYou()
	{
	
	var eepleGender =				["male","female"];
	var eepleShirtColor =			["Black","Blue","Red","Green"];
	var eepleHairColor =			["Black","Blonde","Red","Brown"];
	var eepleSkinColor =			["1","2","3","4"];
	
	var startGender = 				'<ul class="';
	var randomGender = 				eepleGender[Math.floor(Math.random() * eepleGender.length)];
	var endGender = 				'">';
	
	var shadow = 					'<li class="shadow"></li>';
	
	var startBodySkin = 			'<li class="bodySkinTone0';
	var randomBodySkinNumber =		eepleSkinColor[Math.floor(Math.random() * eepleSkinColor.length)];;
	var endBodySkin = 				'"></li>';
	
	var startRandomShirt = 			'<li class="shirtShirt0';
	var randomShirtNumber = 		Math.floor(Math.random()*4) + 1;
	var randomShirt = 				eepleShirtColor[Math.floor(Math.random() * eepleShirtColor.length)];
	var endRandomShirt = 			'"></li>';
	
	var startHeadSkin = 			'<li class="headSkinTone0';
	var randomHeadSkinNumber =	 	randomBodySkinNumber;
	var endHeadSkin = 				'"></li>';
	
	var startRandomHair = 			'<li class="hairHaircut0';
	var randomHairNumber = 			Math.floor(Math.random()*4) +1;
	var randomHair = 				eepleHairColor[Math.floor(Math.random() * eepleHairColor.length)];
	var endRandomHair = 			'"></li>';
	
	var rank = 						'<li class="noRank"></li>';
	
	var endUL = 					'</ul>';
	
	var completePackage =
		startGender +
		randomGender +
		endGender +
		shadow +
		startBodySkin +
		randomBodySkinNumber +
		endBodySkin +
		startRandomShirt +
		randomShirtNumber +
		randomShirt +
		endRandomShirt +
		startHeadSkin +
		randomHeadSkinNumber +
		endHeadSkin +
		startRandomHair +
		randomHairNumber +
		randomHair +
		endRandomHair +
		rank +
		endUL;
	
	return completePackage;
	
	}
	$('.eeple.random').append(randomEepleForYou);
}
// END RANDOM EEPLE
